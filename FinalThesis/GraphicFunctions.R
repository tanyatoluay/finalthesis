getPlot <- function(df, input){
  
  # Plots for Chi-Square Test
  if(input$Model == "chisq"){
    p1 <-  ggplot(df) + geom_mosaic(aes(x=product(var1, Group),fill= Group)) + xlab("") + ylab("") + ggtitle("Mosaic Plot")
    p2 <- ggplot(df, aes(x = var1, fill= Group)) + geom_bar() + xlab("") + ylab("") + ggtitle("Bar Plot")
    plot <- grid.arrange(p1, p2, ncol=2, top= "Plots for Chi-Square Test") 
  }
  
  # Plots for T-Test 
  if (input$Model == "ttest"){
    p <- ggplot(df, aes(x= var2, y= var1, fill= Group))
    p1 <-  p + geom_violin(position = "dodge") + xlab("") + ylab("") + ggtitle("Violin Plot")
    p2 <- p + geom_boxplot() + xlab("") + ylab("") + ggtitle("Box Plot")
    p3 <- p + geom_dotplot(binaxis = "y", stackdir = "center", position = "dodge") + xlab("") + ylab("") + ggtitle("Scatter Plot")
    plot <- grid.arrange(p1, p2, p3, ncol= 2, top= "Plots for Student's T-Test")
  }
  
  # Plots for Anova & MANOVA Tests
  if(input$Model == "anova" | input$Model == "manova"){
    p1 <- ggplot(df, aes(sample = var1, colour= Group)) + stat_qq() +
      stat_qq_line(aes(colour=Group)) + ggtitle("Q-Q Plot")
    p2 <- ggplot(df, aes(x = var1, y = var2)) + geom_jitter(aes(colour = Group), width = 0.1)  + xlab("") + ylab("") + ggtitle("Dot Plot")
    plot <- grid.arrange(p1, p2, ncol= 2, top= "Output Plots")
  }
}