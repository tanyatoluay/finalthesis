**Biostatistics Web Application for Quantitative Data Analysis Using R Shiny**
 
This project is made for the bioinformatics BSc degree final year thesis. 

The web application is built by the R package, Shiny, with the purpose of quantitative data analysis, data exploration, calculating the statistics and creating meaningful plots. It also aims to simplify the process by providing a graphical user interface (GUI) for the researchers. 

The implemented statistical tools include summary statistics, chi-square test, t-test, ANOVA and MANOVA. The application outputs the appropriate plot with the option to download it, either in png or pdf format.  


**Mentor:** Prof. Dr. Uroš Godnov 
